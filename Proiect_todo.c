// TODO IDP

DevOps:

1. trebuie ca proiectul sa functioneze in docker si sa aiba un utilitar
pentru managementul containerelor (2p)
Lab 1 2 3 4 - Docker Swarm persistent

- Dockerfile for MSSQL db ✓
- Dockerfile for be: Java Springboot ✓
- Dockerfile for fe: Node.js ✓
- Docker networking ✓
- Docker bind mounts ✓
- Dockerhub publish images ✓
- Docker compose ✓
- Docker swarm ✓
- Docker NFS (persistenta) ✓

docker run -d --name mssql-data-nfs --privileged -v /database/data:/nfsshare -e SHARED_DIRECTORY=/nfsshare itsthenetwork/nfs-server-alpine:latest
docker init swarm
docker stack deploy -c db_be_compose_swarm.yml help-step-bro-im-stack
docker stack rm help-step-bro-im-stack
docker stack ps help-step-bro-im-stack
docker stack deploy -c db_be_compose_swarm.yml help-step-bro-im-stack


2. trebuie sa folositi rabbitMQ (sau orice alta coada de mesaje) (2p)
Lab 7

go run sendMessage.go
go run recvMessage.go 

vezi stats in browser ✓

3. trebuie sa implementati un sistem de logging al sistemului, cu dashboard
pentru observabilitate (2p)
Lab 7

chmod 777 ./loki
chmod 777 ./loki/wal
docker plugin install grafana/loki-docker-driver:latest --alias loki --grant-all-permissions


Loki ✓
Grafana ✓
Prometheus ✓

4. trebuie ca proiectul vostru sa adere la standardele CI/CD, si sa aiba
minim 3 etape: build, test, deploy (2p)
Lab 6

portainer ✓


5. trebuie ca proiectul vostru sa fie expus printr-un reverse
proxy / api gateway catre exterior (2p - idp)

Lab 4, 5
Kong ✓


schema ppt

